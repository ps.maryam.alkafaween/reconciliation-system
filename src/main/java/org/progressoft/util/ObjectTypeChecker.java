package org.progressoft.util;

import org.progressoft.entity.CsvModelDAO;
import org.progressoft.entity.JsonModelDAO;

public class ObjectTypeChecker {
    private ObjectTypeChecker(){}
    public static boolean isModelDAO(Object obj) {
        return (obj instanceof CsvModelDAO || obj instanceof JsonModelDAO);
    }
}
