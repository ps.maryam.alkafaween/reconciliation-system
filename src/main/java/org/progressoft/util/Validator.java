package org.progressoft.util;

import java.io.File;
import org.apache.commons.io.FilenameUtils;


public class Validator {
    public static boolean formatSupported(String format) {
        return Format.contains(format);
    }
    public static boolean formatMatchesInput(String format, File file) {
        return format.equalsIgnoreCase(FilenameUtils.getExtension(file.getAbsolutePath()));
    }
    public static boolean isValid(File file) {
        return !(file == null || file.getPath().equals("\n") || !file.getAbsoluteFile().exists() || file.isDirectory());
    }
    public static boolean formatConditionsFulfilled(String format, File file) {
        return Validator.formatSupported(format) && Validator.formatMatchesInput(format, file);
    }
}
