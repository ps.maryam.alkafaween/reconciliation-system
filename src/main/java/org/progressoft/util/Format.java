package org.progressoft.util;

import java.util.NoSuchElementException;

public enum Format {
    CSV, JSON;

    public static boolean contains(String format) {
        Format[] formats = Format.values();
        for (Format curFormat : formats) {
            if (format.equalsIgnoreCase(curFormat.name())) {
                return true;
            }
        }
        return false;
    }

    public static Format parse(String format) {
        Format[] formats = Format.values();
        for (Format curFormat : formats) {
            if (format.equalsIgnoreCase(curFormat.name())) {
                return curFormat;
            }
        }
        throw new NoSuchElementException("Format does not exist");
    }
}
