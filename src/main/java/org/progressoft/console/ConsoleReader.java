package org.progressoft.console;

import lombok.SneakyThrows;
import org.progressoft.util.FileType;
import org.progressoft.util.Format;

import java.io.File;
import java.nio.file.FileSystemException;
import java.util.NoSuchElementException;
import java.util.Scanner;

import static org.progressoft.console.ConsoleDisplay.promptForFile;
import static org.progressoft.console.ConsoleDisplay.promptForFormat;
import static org.progressoft.util.Validator.*;

public class ConsoleReader {
    private final static Scanner sc;
    static {
        sc = new Scanner(System.in);
    }
    public static Format getValidFormat(FileType type, File file) {
        promptForFormat(type);
        String format;
        try {
            format = sc.nextLine();
        } catch (NoSuchElementException e) {
            throw new NullPointerException("No Input was found.");
        } catch (IllegalStateException e) {
            throw new IllegalStateException("Input scanner is closed.");
        }
        if (!formatConditionsFulfilled(format, file))
            throw new NoSuchElementException("Extension is either not supported or does not match file extension.");
        return Format.parse(format);
    }

    @SneakyThrows
    public static File getValidFile(FileType type) {
        promptForFile(type);
        File file;
        try {
            file = new File(sc.nextLine());
        } catch (NoSuchElementException e) {
            throw new NullPointerException("No Input was found.");
        } catch (IllegalStateException e) {
            throw new IllegalStateException("Input scanner is closed.");
        }
        if (!isValid(file))
            throw new FileSystemException("File either does not exist, is null, or is a directory.");
        return file;
    }
}
