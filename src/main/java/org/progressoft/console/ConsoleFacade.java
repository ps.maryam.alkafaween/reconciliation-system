package org.progressoft.console;

public interface ConsoleFacade {
    void run();
}
