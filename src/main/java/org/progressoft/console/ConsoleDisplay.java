package org.progressoft.console;

import org.progressoft.util.FileType;

public class ConsoleDisplay {
    public static void promptForFile(FileType type) {
        System.out.println("Enter " + type.name() + " file location:");
    }
    public static void promptForFormat(FileType type) {
        System.out.println("Enter " + type.name() + " file format:");
    }
}
