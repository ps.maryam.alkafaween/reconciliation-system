package org.progressoft.console;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.progressoft.entity.ModelDAO;
import org.progressoft.resultmanager.CsvResultManager;
import org.progressoft.resultmanager.ResultManager;
import org.progressoft.parser.Parser;
import org.progressoft.parser.ParserFactory;
import org.progressoft.util.FileType;
import org.progressoft.util.Format;


import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystemException;
import java.util.List;
import java.util.NoSuchElementException;

import static org.progressoft.console.ConsoleReader.getValidFile;
import static org.progressoft.console.ConsoleReader.getValidFormat;

public class FileSystemConsoleFacade implements ConsoleFacade {
    @Override
    @SneakyThrows
    public void run() {
        Parser parser;
        List<ModelDAO> sourceRecords;
        List<ModelDAO> targetRecords;
        try {
            File source = getValidFile(FileType.SOURCE);
            Format sourceFormat = getValidFormat(FileType.SOURCE, source);
            parser = ParserFactory.createParser(sourceFormat);
            sourceRecords = parser.parse(source.getAbsolutePath(), FileType.SOURCE);

            File target = getValidFile(FileType.TARGET);
            Format targetFormat = getValidFormat(FileType.TARGET, target);
            parser = ParserFactory.createParser(targetFormat);
            targetRecords = parser.parse(target.getAbsolutePath(), FileType.TARGET);
        } catch (NoSuchElementException e) {
            throw new NoSuchElementException("Extension does not exist");
        } catch (NullPointerException e) {
            throw new NullPointerException("Format requested is null.");
        }

        ResultManager resultManager = new CsvResultManager();
        try {
            File directory = new File("src/main/resources/result-files");
            FileUtils.cleanDirectory(directory);
        } catch (IOException e) {
            throw new FileSystemException("Erasing previous existing files went unsuccessful.");
        } catch (IllegalArgumentException e) {
            throw new NoSuchElementException("Directory does not exist or is not a directory.");
        }

        compareRecords(sourceRecords, targetRecords, resultManager);
        checkForMissingRecords(sourceRecords, targetRecords, resultManager);
        checkForMissingRecords(targetRecords, sourceRecords, resultManager);
    }

    private static void checkForMissingRecords(List<ModelDAO> sourceRecords, List<ModelDAO> targetRecords, ResultManager resultManager) {
        for (ModelDAO targetRecord : targetRecords) {
            boolean isMissing = true;
            for (ModelDAO sourceRecord : sourceRecords) {
                if (sourceRecord.sameTransactionAs(targetRecord))
                    isMissing = false;
            }
            if (isMissing)
                resultManager.addToMissingRecords(targetRecord);
        }
    }

    private static void compareRecords(List<ModelDAO> sourceRecords, List<ModelDAO> targetRecords, ResultManager resultManager) {
        for (ModelDAO sourceRecord : sourceRecords) {
            for (ModelDAO targetRecord : targetRecords) {
                if (sourceRecord.sameTransactionAs(targetRecord)) {
                    if (sourceRecord.equals(targetRecord))
                        resultManager.addToMatchingRecords(sourceRecord);
                    else {
                        resultManager.addToMismatchingRecords(sourceRecord);
                        resultManager.addToMismatchingRecords(targetRecord);
                    }
                }
            }
        }
    }
}
