package org.progressoft.parser;

import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.exceptions.CsvException;
import lombok.SneakyThrows;
import org.progressoft.entity.*;
import org.progressoft.util.FileType;

import java.io.*;
import java.util.*;


public class CsvParser implements Parser {
    @Override
    @SneakyThrows
    public List<ModelDAO> parse(String fileName, FileType fileType) {
        List<ModelDAO> csvRecords = new ArrayList<>();
        try (CSVReader csvReader = new CSVReader(new FileReader(fileName))) {
            List<CsvModel> beans = new CsvToBeanBuilder<CsvModel>(csvReader)
                    .withType(CsvModel.class)
                    .withExceptionHandler(e -> csvRequiredFieldEmptyExceptionHandler())
                    .build()
                    .parse();
            for (CsvModel csvModel : beans) {
                csvModel.setFoundInFile(fileType);
                csvRecords.add(new CsvModelDAO(csvModel));
            }
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("Problems encountered while trying to read file. File is possibly a directory or does not exist.");
        } catch (IllegalStateException e) {
            throw new CsvException("Problems encountered while building CSV parser.");
        }   catch (RuntimeException e) {
            throw new IOException("Problems encountered while trying to read or parse the file.");
        } catch (IOException e) {
            throw new IOException("Problems encountered while trying to read file.");
        }
        return csvRecords;
    }
    @SneakyThrows
    private CsvException csvRequiredFieldEmptyExceptionHandler() {
        return new CsvException("Format of fields does not match required format.");
    }
}
