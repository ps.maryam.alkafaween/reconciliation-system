package org.progressoft.parser;

import lombok.SneakyThrows;
import org.progressoft.util.Format;

import java.util.NoSuchElementException;

public class ParserFactory {
    private ParserFactory() {}
    @SneakyThrows
    public static Parser createParser(Format format) {
        if (format == null)
            throw new NullPointerException();
        if (format.equals(Format.CSV))
            return new CsvParser();
        else if (format.equals(Format.JSON))
            return new JsonParser();
        throw new NoSuchElementException("Requested format is not supported.");
    }
}
