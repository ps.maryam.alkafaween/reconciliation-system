package org.progressoft.parser;

import org.progressoft.entity.ModelDAO;
import org.progressoft.util.FileType;

import java.util.List;

public interface Parser {
    List<ModelDAO> parse(String fileName, FileType fileType);
}
