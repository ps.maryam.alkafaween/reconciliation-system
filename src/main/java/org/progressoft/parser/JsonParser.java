package org.progressoft.parser;

import lombok.SneakyThrows;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.progressoft.entity.JsonModel;
import org.progressoft.entity.JsonModelDAO;
import org.progressoft.entity.ModelDAO;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.IllegalFormatFlagsException;
import java.util.List;
import org.json.simple.parser.JSONParser;
import org.progressoft.util.FileType;

public class JsonParser implements Parser {
    @SneakyThrows
    public List<ModelDAO> parse(String fileName, FileType fileType) {
        List<ModelDAO> jsonRecords = new ArrayList<>();
        try {
        FileReader fileReader = new FileReader(fileName);
        JSONArray arr = (JSONArray) (new JSONParser()).parse(fileReader);
            for (Object entry: arr) {
                JSONObject jsonRecord = (JSONObject) entry;
                JsonModel model = createJsonModel(fileType, jsonRecord);
                jsonRecords.add(new JsonModelDAO(model));
            }
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("JSON file could not be found.");
        } catch (IOException | ParseException e) {
            throw new IOException("JSON file could not be parsed.");
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Problems encountered while parsing numbers.");
        } catch (DateTimeParseException e) {
            throw new DateTimeException("Given date could not be parsed.");
        } catch (ClassCastException e) {
            throw new IllegalFormatFlagsException("Format of file does not match required format.");
        } catch (NullPointerException e) {
            throw new NoSuchFieldException("Fields included in file do not match required fields.");
        }
        return jsonRecords;
    }

    private JsonModel createJsonModel(FileType fileType, JSONObject jsonRecord) {
        String reference = (String) jsonRecord.get("reference");
        double amount = Double.parseDouble((String) jsonRecord.get("amount"));
        String currencyCode = (String) jsonRecord.get("currencyCode");
        String purpose = (String) jsonRecord.get("purpose");
        LocalDate valueDate = getJsonDate(jsonRecord);
        return new JsonModel(fileType, valueDate, reference, amount, currencyCode, purpose);
    }

    private LocalDate getJsonDate(JSONObject jsonRecord) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return LocalDate.parse((String) jsonRecord.get("date"), df);
    }
}
