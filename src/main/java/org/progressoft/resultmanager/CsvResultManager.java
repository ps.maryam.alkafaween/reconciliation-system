package org.progressoft.resultmanager;
import lombok.SneakyThrows;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.progressoft.entity.ModelDAO;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class CsvResultManager implements ResultManager {
    @Override
    public void addToMatchingRecords(ModelDAO record) {
        final String MATCHING_FILE_PATH = "src/main/resources/result-files/matching-transactions.csv";
        addRecordToFile(record, MATCHING_FILE_PATH);
    }

    @Override
    public void addToMismatchingRecords(ModelDAO record) {
        final String MISMATCHING_FILE_PATH = "src/main/resources/result-files/mismatched-transactions.csv";
        addRecordToFile(record, MISMATCHING_FILE_PATH);
    }

    @Override
    public void addToMissingRecords(ModelDAO record) {
        final String MISSING_FILE_PATH = "src/main/resources/result-files/missing-transactions.csv";
        addRecordToFile(record, MISSING_FILE_PATH);
    }
    @SneakyThrows
    private static void addRecordToFile(ModelDAO record, String fileName) {
        boolean existed = new File(fileName).exists();
        CSVPrinter csvPrinter;
        try {
            FileWriter writer = new FileWriter(fileName, true);
            if (!existed)
                csvPrinter = new CSVPrinter(writer, CSVFormat.Builder.create().setHeader("found in file", "transaction id", "amount", "currency code", "value date").build());
            else csvPrinter = new CSVPrinter(writer, CSVFormat.Builder.create().build());
            csvPrinter.printRecord(record.getFoundInFile(), record.getId(), record.getAmount(), record.getCurrency(), record.getDate());
            csvPrinter.flush();
        } catch (IOException e) {
            throw new IOException("Could not write results to a CSV file.");
        }
    }
}
