package org.progressoft.resultmanager;

import org.progressoft.entity.ModelDAO;

public interface ResultManager {
    void addToMatchingRecords(ModelDAO record);
    void addToMismatchingRecords(ModelDAO record);
    void addToMissingRecords(ModelDAO record);
}
