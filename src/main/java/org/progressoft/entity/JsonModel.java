package org.progressoft.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.progressoft.util.FileType;
import java.time.LocalDate;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class JsonModel {
    private FileType foundInFile;
    private LocalDate date;
    private String reference;
    private double amount;
    private String currencyCode;
    private String purpose;

}
