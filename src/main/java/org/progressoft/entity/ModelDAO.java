package org.progressoft.entity;
import org.progressoft.util.FileType;
import java.time.LocalDate;

import static org.progressoft.util.ObjectTypeChecker.isModelDAO;

public interface ModelDAO {
    FileType getFoundInFile();
    String getId();
    double getAmount();
    String getCurrency();
    LocalDate getDate();
    @Override
    boolean equals(Object obj);
    default boolean sameTransactionAs(Object obj) {
        if (obj == this)
            return true;
        if (!isModelDAO(obj))
            return false;
        ModelDAO model = (ModelDAO) obj;
        return model.getId().equals(this.getId());
    }

}
