package org.progressoft.entity;
import org.progressoft.util.FileType;
import java.time.LocalDate;

import static org.progressoft.util.ObjectTypeChecker.isModelDAO;

public class CsvModelDAO implements ModelDAO{
    CsvModel model;
    public CsvModelDAO(CsvModel model) {
        this.model = model;
    }
    @Override
    public FileType getFoundInFile() {
        return model.getFoundInFile();
    }

    @Override
    public String getId() {
        return model.getTransUniqueId();
    }

    @Override
    public double getAmount() {
        return model.getAmount();
    }

    @Override
    public String getCurrency() {
        return model.getCurrency();
    }

    @Override
    public LocalDate getDate() {
        return model.getValueDate();
    }
    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (!isModelDAO(obj))
            return false;
        ModelDAO model = (ModelDAO) obj;
        return sameTransactionAs(model) && model.getAmount() == this.getAmount() && model.getCurrency().equalsIgnoreCase(this.getCurrency()) && this.getDate().equals(model.getDate());
    }
}
