package org.progressoft.entity;

import org.progressoft.util.FileType;

import java.time.LocalDate;

import static org.progressoft.util.ObjectTypeChecker.isModelDAO;

public class JsonModelDAO implements ModelDAO{
    JsonModel model;
    public JsonModelDAO(JsonModel model) {
        this.model = model;
    }
    @Override
    public FileType getFoundInFile() {
        return model.getFoundInFile();
    }

    @Override
    public String getId() {
        return model.getReference();
    }

    @Override
    public double getAmount() {
        return model.getAmount();
    }

    @Override
    public String getCurrency() {
        return model.getCurrencyCode();
    }

    @Override
    public LocalDate getDate() {
        return model.getDate();
    }
    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (!isModelDAO(obj))
            return false;
        ModelDAO model = (ModelDAO) obj;
        return sameTransactionAs(model) && model.getAmount() == this.getAmount() && model.getCurrency().equalsIgnoreCase(this.getCurrency()) && this.getDate().equals(model.getDate());
    }
}
