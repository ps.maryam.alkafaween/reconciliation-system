package org.progressoft.entity;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvDate;
import lombok.*;
import org.progressoft.util.FileType;
import org.progressoft.util.TransType;

import java.time.LocalDate;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CsvModel {
    @Setter
    private FileType foundInFile;
    @CsvBindByName(column = "trans unique id")
    private String transUniqueId;
    @CsvBindByName(column = "trans description")
    private String transDescription;
    @CsvBindByName(column = "amount")
    private double amount;
    @CsvBindByName(column = "currency")
    private String currency;
    @CsvBindByName(column = "purpose")
    private String purpose;
    @CsvDate(value = "yyyy-MM-dd")
    @CsvBindByName(column = "value date")
    private LocalDate valueDate;
    @CsvBindByName(column = "trans type")
    private TransType transType;
}
