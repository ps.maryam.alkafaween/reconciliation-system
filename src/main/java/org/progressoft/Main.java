package org.progressoft;

import org.progressoft.console.FileSystemConsoleFacade;
public class Main {
    public static void main(String[] args) {
        new FileSystemConsoleFacade().run();
    }
}