package org.progressoft.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.progressoft.util.FileType;
import org.progressoft.util.TransType;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.*;

public class CsvModelDAOTest {
    ModelDAO record1, record2, record3, record4;
    @BeforeEach
    void setup() {
        DateTimeFormatter df1 = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        DateTimeFormatter df2 = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        record1 = new CsvModelDAO(new CsvModel(FileType.TARGET, "1234", "description", 54.4, "someCur", "purpose", LocalDate.parse("20-01-2023", df1), TransType.D));
        record2 = new CsvModelDAO(new CsvModel(FileType.SOURCE, "1234", "anotherDesc", 54.4, "someCur", "anotherPurpose", LocalDate.parse("20/01/2023", df2), TransType.C));
        record3 = new JsonModelDAO(new JsonModel(FileType.SOURCE, LocalDate.parse("20/01/2023", df2), "1234", 54.4, "someCur", "anotherPurpose"));
        record4 = new JsonModelDAO(new JsonModel(FileType.TARGET, LocalDate.parse("20-01-2022", df1), "1234", 54.4, "someCur", "purpose"));
    }
    @Test
    void givenTwoCsvEqualModels_whenComparing_thenTrueIsReturned() {
        assertEquals(record1, record2);
    }
    @Test
    void givenMatchingCsvAndJsonRecords_whenComparing_thenFalseIsReturned() {
        assertEquals(record1, record3);
    }
    @Test
    void givenCsvAndJsonMismatchingIdRecords_whenComparing_thenFalseIsReturned() {
        assertNotEquals(record1, record4);
    }
}
