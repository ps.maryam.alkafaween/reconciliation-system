package org.progressoft.resultmanager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.progressoft.entity.*;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

public class CsvResultManagerTest {
    ResultManager manager;
    ModelDAO jsonRecord, csvRecord;
    @BeforeEach
    void setup() {
        manager = new CsvResultManager();
        csvRecord =  new CsvModelDAO(new CsvModel());
        jsonRecord = new JsonModelDAO(new JsonModel());
    }
    @Test
    void givenACsvRecord_whenAddingItToMatchingFile_thenItAddsSuccessfully() {
        assertDoesNotThrow(() -> manager.addToMatchingRecords(csvRecord));
    }
    @Test
    void givenACsvRecord_whenAddingItToMismatchingFile_thenItAddsSuccessfully() {
        assertDoesNotThrow(() -> manager.addToMismatchingRecords(csvRecord));
    }
    @Test
    void givenACsvRecord_whenAddingItToMissingFile_thenItAddsSuccessfully() {
        assertDoesNotThrow(() -> manager.addToMissingRecords(csvRecord));
    }
    @Test
    void givenAJsonRecord_whenAddingItToMatchingFile_thenItAddsSuccessfully() {
        assertDoesNotThrow(() -> manager.addToMatchingRecords(jsonRecord));
    }
    @Test
    void givenAJsonRecord_whenAddingItToMismatchingFile_thenItAddsSuccessfully() {
        assertDoesNotThrow(() -> manager.addToMismatchingRecords(jsonRecord));
    }
    @Test
    void givenAJsonRecord_whenAddingItToMissingFile_thenItAddsSuccessfully() {
        assertDoesNotThrow(() -> manager.addToMissingRecords(jsonRecord));
    }
}
