package org.progressoft.parser;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.progressoft.entity.ModelDAO;
import org.progressoft.util.FileType;

import java.io.FileNotFoundException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class CsvParserTest {
    private Parser parser;
    @BeforeEach
    void setup() {
        parser = new CsvParser();
    }
    @Test
    void givenCsvFile_whenParsingIt_thenParsesSuccessfully() {
        String fileName = "src/main/resources/test-input-files/bank-transactions.csv";
        assertDoesNotThrow(() -> parser.parse(fileName, FileType.SOURCE));
    }

    @Test
    void givenValidCsvFile_whenParsingFile_thenListOfCsvModelsIsProduced() {
        String fileName = "src/main/resources/test-input-files/bank-transactions.csv";
        List<ModelDAO> records = parser.parse(fileName, FileType.SOURCE);
        assertEquals(6, records.size());
    }

    @Test
    void givenNonExistentFile_whenParsingFile_thenExceptionIsHandled() {
        String fileName = "src/main/resources/test-input-files/nonExistentFile";
        assertThrows(FileNotFoundException.class, () -> parser.parse(fileName, FileType.SOURCE));
    }
    @Test
    void givenCsvFileWithUnexpectedAttributes_whenParsingFile_thenNoExceptionIsThrown() {
        String fileName = "src/main/resources/test-input-files/unexpected-attributes.csv";
        assertDoesNotThrow(() -> parser.parse(fileName, FileType.SOURCE));
    }
    @Test
    void givenNonCsvFile_whenParsingFile_thenNoExceptionIsThrown() {
        String fileName = "src/main/resources/test-input-files/bank-transactions.json";
        parser.parse(fileName, FileType.SOURCE);
        assertDoesNotThrow(() -> parser.parse(fileName, FileType.SOURCE));
    }
    @Test
    void givenNonConsistentCsvFile_whenParsingFile_thenNoExceptionIsThrown() {
        String fileName = "src/main/resources/test-input-files/non-consistent-format.csv";
        assertDoesNotThrow(() -> parser.parse(fileName, FileType.SOURCE));
    }
    @Test
    void givenJsonFileWithInvalidDate_whenParsingFile_thenNoExceptionIsThrown() {
        String fileName = "src/main/resources/test-input-files/non-numeric-amount.csv";
        assertDoesNotThrow(() -> parser.parse(fileName, FileType.SOURCE));
    }
    @Test
    void givenCsvFileWithUnexpectedDateFormat_whenParsingFile_thenNoExceptionIsThrown() {
        String fileName = "src/main/resources/test-input-files/unexpected-date-format.csv";
        assertDoesNotThrow(() -> parser.parse(fileName, FileType.SOURCE));
    }
}