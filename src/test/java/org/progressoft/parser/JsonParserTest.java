package org.progressoft.parser;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.progressoft.entity.ModelDAO;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.DateTimeException;
import java.util.IllegalFormatFlagsException;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
import org.progressoft.util.FileType;

public class JsonParserTest {
    private Parser parser;
    @BeforeEach
    void setup() {
        parser = new JsonParser();
    }
    @Test
    void givenJsonFile_whenParsingIt_thenParsesSuccessfully() {
        String fileName = "src/main/resources/test-input-files/online-banking-transactions.json";
        assertDoesNotThrow(()-> parser.parse(fileName, FileType.SOURCE));
    }
    @Test
    void givenValidJsonFileName_whenParsingFile_thenListOfJsonModelsIsProduced() {
        String fileName = "src/main/resources/test-input-files/online-banking-transactions.json";
        List<ModelDAO> records = parser.parse(fileName, FileType.SOURCE);
        assertEquals(7, records.size());
    }
    @Test
    void givenNonExistentFile_whenParsingFile_thenFileNotFoundExceptionIsThrown() {
        String fileName = "src/main/resources/test-input-files/nonExistentFile";
        assertThrows(FileNotFoundException.class, () -> parser.parse(fileName, FileType.SOURCE));
    }

    @Test
    void givenJsonFileWithDifferentAttributes_whenParsingFile_thenNoSuchFieldExceptionIsThrown() {
        String fileName = "src/main/resources/test-input-files/unexpected-attributes.json";
        assertThrows(NoSuchFieldException.class, () -> parser.parse(fileName, FileType.SOURCE));
    }

    @Test
    void givenNonJsonFile_whenParsingFile_thenIOExceptionIsThrown() {
        String fileName = "src/main/resources/test-input-files/bank-transactions.csv";
        assertThrows(IOException.class, () -> parser.parse(fileName, FileType.SOURCE));

    }
    @Test
    void givenJsonFileWithUnexpectedFormat_whenParsingFile_thenIllegalFormatFlagsExceptionIsThrown() {
        String fileName = "src/main/resources/test-input-files/unexpected-format.json";
        assertThrows(IllegalFormatFlagsException.class, () -> parser.parse(fileName, FileType.SOURCE));
    }
    @Test
    void givenJsonFileWithNonNumericAmount_whenParsingFile_thenNumberFormatExceptionIsThrown() {
        String fileName = "src/main/resources/test-input-files/non-numeric-amount.json";
        assertThrows(NumberFormatException.class, () -> parser.parse(fileName, FileType.SOURCE));
    }
    @Test
    void givenJsonFileWithInvalidDate_whenParsingFile_thenDateTimeExceptionIsThrown() {
        String fileName = "src/main/resources/test-input-files/invalid-date.json";
        assertThrows(DateTimeException.class, () -> parser.parse(fileName, FileType.SOURCE));
    }
}
