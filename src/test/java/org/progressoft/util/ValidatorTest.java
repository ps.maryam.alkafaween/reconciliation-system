package org.progressoft.util;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import static org.junit.jupiter.api.Assertions.*;


public class ValidatorTest {
    @Test
    void givenExistentFile_whenObtainingValidity_thenTrueIsReturned() {
        File f = new File("src/main/resources/test-input-files/bank-transactions.csv");
        assertTrue(Validator.isValid(f));
    }
    @Test
    void givenNonExistentFile_whenObtainingValidity_thenFalseIsReturned() {
        File f = new File("src/main/resources/test-input-files/nonExistentFile");
        assertFalse(Validator.isValid(f));
    }
    @Test
    void givenNullFile_whenObtainingValidity_thenFalseIsReturned() {
        assertFalse(Validator.isValid(null));
    }
    @Test
    void givenDirectory_whenObtainingValidity_thenFalseIsReturned() {
        File f = new File("src");
        assertTrue(f.isDirectory());
        assertFalse(Validator.isValid(f));
    }
    @Test
    void givenVeryLongFileName_whenCreatingFile_thenNoExceptionIsThrown() {
        assertDoesNotThrow(this::createFileWithOversizeName);
    }
    @ParameterizedTest
    @ValueSource(strings = {"csv", "json", "CSV", "JSON", "jSoN", "CsV"})
    void givenSupportedFormats_whenValidatingSupport_thenTrueIsReturned(String format) {
        assertTrue(Validator.formatSupported(format));
    }
    @ParameterizedTest
    @ValueSource(strings = {"unknownExtension", "JASON", "CeeSV"})
    void givenUnsupportedFormats_whenValidatingSupport_thenFalseIsReturned(String format) {
        assertFalse(Validator.formatSupported(format));
    }
    @Test
    void givenMatchingFileAndFormat_whenValidatingMatching_thenTrueIsReturned() {
        Map<File, String> fileFormats = new HashMap<>();
        fileFormats.put(new File("file.csv"), "csv");
        fileFormats.put(new File("file.csv"), "CsV");
        fileFormats.put(new File("file.CSv"), "csv");
        fileFormats.put(new File("file.json"), "json");
        fileFormats.put(new File("file.JSOn"), "jSoN");
        fileFormats.put(new File("file.something"), "SOMETHING");
        boolean allMatch = true;
        for (Map.Entry<File, String> entry : fileFormats.entrySet()) {
            allMatch &= Validator.formatMatchesInput(entry.getValue(), entry.getKey());
        }
        assertTrue(allMatch);
    }
    @Test
    void givenNonMatchingFileAndFormat_whenValidatingMatching_thenFalseIsReturned() {
        Map<File, String> fileFormats = new HashMap<>();
        fileFormats.put(new File("file.csv"), "anotherFormat");
        fileFormats.put(new File("file.json"), "anotherFormat");
        fileFormats.put(new File("file.something"), "anotherFormat");
        boolean oneMatchAtLeastFound = false;
        for (Map.Entry<File, String> entry : fileFormats.entrySet()) {
            oneMatchAtLeastFound |= Validator.formatMatchesInput(entry.getValue(), entry.getKey());
        }
        assertFalse(oneMatchAtLeastFound);
    }
    private File createFileWithOversizeName() {
        String name = StringUtils.repeat("*", 300);
        return new File(name);
    }
}
